function dijkstra(graph, start) {
    const distances = {};
    const visited = new Set();
  
    // Initialiser les distances
    for (let vertex in graph) {
      distances[vertex] = Infinity;
    }
    distances[start] = 0;
  
    while (true) {
      // Trouver le sommet non visité avec la plus petite distance
      let currentVertex = null;
      let currentDistance = Infinity;
      for (let vertex in distances) {
        if (!visited.has(vertex) && distances[vertex] < currentDistance) {
          currentVertex = vertex;
          currentDistance = distances[vertex];
        }
      }
  
      // Si tous les sommets ont été visités, on a terminé
      if (currentVertex === null) {
        break;
      }
  
      // Marquer le sommet comme visité
      visited.add(currentVertex);
  
      // Mettre à jour les distances
      for (let neighbor in graph[currentVertex]) {
        const distance = distances[currentVertex] + graph[currentVertex][neighbor];
        if (distance < distances[neighbor]) {
          distances[neighbor] = distance;
        }
      }
    }
  
    return distances;
  }
  
  // Exemple d'utilisation
  const graph = {
    'A': { 'B': 4, 'C': 2 },
    'B': { 'A': 4, 'C': 5, 'D': 10 },
    'C': { 'A': 2, 'B': 5, 'D': 3 },
    'D': { 'B': 10, 'C': 3 }
  };
  
  console.log(dijkstra(graph, 'A'));
  // Output: { A: 0, B: 4, C: 2, D: 5 }